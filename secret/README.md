# The Secret Folder

This MUST NOT be accessible by normal system users or the web server.

Should use 770 permissions and be owned by www-data:www-data.
