<?php
function clearCookies() {
    if(count($_COOKIE) > 0) {
        foreach ($_COOKIE as $key => $value) {
            setcookie($key, "", time()-3600);
        }
    }
}


function flog($log_msg) {
    $log_filename = "/var/www/html";
    $log_file_data = $log_filename.'/log_' . date('d-M-Y') . '.log';
    file_put_contents($log_file_data, $log_msg . "\n", FILE_APPEND);
}

function enableDebug(){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

function returnError($error){
    if (gettype($error) == "object") {
        $error = json_encode($error);
    }
    header('Content-type: application/json');
    echo json_encode(array("error" => $error));
    exit();
}

function returnSuccess($success, $refreshToken = ""){
    header('Content-type: application/json');
    echo json_encode(array(
        "data" => $success,
        "refreshToken" => $refreshToken
    ));
    exit();
}

function getUsers(){
    return explode("\n", shell_exec("cut -d: -f1 /etc/passwd"));
}

function userExists($user){
    return in_array($user, getUsers());
}

function geminiProxy($user, $action){
    return shell_exec("/usr/bin/sudo /etc/ttyserver/bin/toggleProxy ".$action." \"".$user."\"");
}
?>
