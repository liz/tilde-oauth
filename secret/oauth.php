<?php

function verifyCredentials($Auth) {
    $request = curl_init();
    curl_setopt($request, CURLOPT_URL, "https://hackers.town/api/v1/accounts/verify_credentials");
    curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($request, CURLOPT_HTTPHEADER, array(
        "Authorization: Bearer ".$Auth
    ));
    $response = curl_exec($request);
    curl_close($request);
    $User = json_decode($response);
    return (isset($User->error) ? $User->error : $User);
}

function oauthToken($AuthCode, $config){
    $AuthToken = "";
    $UserName = "";
    $ErrorDesc = "";
    $UserId = "";
    $request = curl_init();
    curl_setopt($request, CURLOPT_POST, 1);
    curl_setopt($request, CURLOPT_URL, "https://hackers.town/oauth/token");
    curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
    $origin = "https://tty.hackers.town";
    if(file_exists("/var/www/usergen/DOMAIN_OVERRIDE")){
        $origin = str_replace("\n", "", file_get_contents("/var/www/usergen/DOMAIN_OVERRIDE"));
    }
    $redirectUri = $origin."/auth";
    $options = "grant_type=authorization_code&code=".$AuthCode."&client_id=".$config->oauth->key."&client_secret=".$config->oauth->secret."&scope=read:accounts&redirect_uri=".$redirectUri;
    curl_setopt($request, CURLOPT_POSTFIELDS, $options);
    $response = curl_exec($request);
    curl_close($request);
    $Auth = json_decode($response);
    return (isset($Auth->error) ? $Auth->error_description : $Auth);
}

function getHello() {
    $Welcomes = array("Welcome", "Dobrodošli", "Vitejte", "Welkom", "Tervetuloa", "Willkommen", "Fáilte", "Benvenuto", "Bienvenidos", "Välkommen", "ようこそ");
    return $Welcomes[array_rand($Welcomes)];
}
?>
