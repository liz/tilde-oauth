var DEBUG = false;
var isMobile = false;
var USE_ORIGIN = "";

const SwalConfig = {
  color: "#79F257",
  background: "#022601",
  buttonsStyling: false,
}

const isOverflown = ({ clientHeight, scrollHeight }) => scrollHeight > clientHeight

const resizeText = ({ element, elements, minSize = 10, maxSize = 512, step = 1, unit = 'px' }) => {
  (elements || [element]).forEach(el => {
    let i = minSize;
    let overflow = false;
    const parent = el.parentNode;
    while (!overflow && i < maxSize) {
      el.style.fontSize = `${i}${unit}`;
      overflow = isOverflown(parent);
      if (!overflow) i += step;
    }
    // revert to last state where no overflow happened
    el.style.fontSize = `${i - step}${unit}`;
  });
}

const disableNonDesktopElements = () => {
  var disableElements = document.getElementsByClassName("desktopOnly");
  for(var i=0; i< disableElements.length; i++){
    var gutter = disableElements.item(i);
    gutter.classList.remove("col-4");
    gutter.classList.add("col-1");
  }
  var content = document.getElementById("content");
  content.style.marginTop = "6vw";
  content.classList.remove("col-4");
  content.classList.add("col-10");
  var te = document.getElementById("resizer");
  window.fitText(te);
  var buttons = document.getElementsByClassName("keyButton");
  for(var i=0; i<buttons.length; i++){
    var bttn = buttons.item(i);
    bttn.style.height = "15vw";
  }
}

const failMsg = (msg) => {
  Swal.fire({
    ...SwalConfig,
    title: "Error!",
    text: msg,
  }).then(() => {
    window.location.replace("/");
  });
}

const beginOauth = () => {
  $.ajax({
    url: USE_ORIGIN+"/auth/api?act=id"
  }).then((data) => {
    if(data.id){
      var redirect = "https://hackers.town/oauth/authorize?"+
        "response_type=code&client_id="+data.id+"&redirect_uri="+
        USE_ORIGIN+"/auth&scope=read:accounts";
      window.location.href = redirect;
    }else{
      // Auth Failed
      failMsg("Server ID Error");
    }
  });
}

const displayFingerprints = () => {
  // Get SSH Fingerprints and display them
  $.get(USE_ORIGIN+"/fingerprint.php", (response) => {
    if (response) {
      var html = "<div><table class=\"fingerprintTable\"><tr><th>Bits</th><th>Fingerprint (SHA256)</th><th class=\"text-start\">Algorithm</th></tr>";
      response.split("\n").forEach((line) => {
        var parts = line.split(" ");
        if(parts.length === 4){
          html += "<tr><td class=\"fingerprintBit\">"+parts[0]+"</td><td class=\"fingerprintData\">"+parts[1].replace("SHA256:", "")+"</td><td class=\"fingerprintAlgo\">"+parts[3].replace("(", "").replace(")", "")+"</td></tr>";
        }
      });
      html += "</table></div>";
      Swal.fire({
        ...SwalConfig,
        title: "SSH Fingerprints",
        html: html
      });
    } else {
      Swal.fire({
        ...SwalConfig,
        title: "Unable to Lookup SSH Fingerprints",
        text: response.error
      });
    }
  });
}

// On Page Load...
$(() => {
  var token = localStorage.getItem("tty_token");
  if (token && token !== "undefined") {
    window.location.href = "/auth";
  }
  // Override domain
  $.get("/DOMAIN_OVERRIDE", function (data) {
      USE_ORIGIN = data.replaceAll("\n", "");
      DEBUG = true;
    }).fail(() => {
      USE_ORIGIN = "https://tty.hackers.town";
    }).always(() => {

      // Device Detection
      if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
        /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        isMobile = true;
      }
      // Adjust for Mobile
      if(isMobile){
        disableNonDesktopElements();
      }
      // Console Welcome
      console.log("%cWelcome Hacker!", "color: #ff0000; font-size: 7em; font-style: italic; font-family: 'Times New Roman', Times, serif;");
      // Enable Extra Debug Stuff
      if(DEBUG){
        $('.debug').each((i,e)=>{
          e.style.display = "unset";
        });
      }
      var parameters = new URL(window.location.href).searchParams;
      var message = parameters.get("msg");
      var error = parameters.get("error");
      if(error){
        failMsg(error);
      }
      if(message){
        Swal.fire({
          ...SwalConfig,
          text: message
        }).then(() => {
          window.location.replace("/");
        });
      }

    })
});
