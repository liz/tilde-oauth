<?php
    $config = json_decode(file_get_contents("/var/www/usergen/secret/config.json", true));
    require_once("/var/www/usergen/secret/helpers.php");
    require_once("/var/www/usergen/secret/oauth.php");
?>
<!DOCTYPE html>
<HTML lang="en">
    <?php require("/var/www/usergen/elements/head.php"); ?>
    <Body>
        <script src="/base64url.js"></script>
        <script src="/ssh-util.js"></script>
        <script src="/keygen.js"></script>
        <script src="/auth/auth.js"></script>
        <div class="row">
            <div class="desktopOnly col-4"></div>
            <div id="content" class="col-4 center">
                <?php require("/var/www/usergen/elements/logo.php"); ?>
                <div class="row">
                    <span>
                        <?php
                            echo getHello();
                        ?>
                    </span>
                    <span id="resizer" class="name">
                        Fellow Townie
                    </span>
                </div>
                <div class="row button">
                    <button class="col keyButton" onclick="generateSSH()">Generate Keypair</button>
                    <button class="col keyButton" onclick="uploadSSH()">Upload Pubkey</button>
                    <button class="col keyButton debug" onclick="testSwal()">Test Popup</button>
                    <form id="uploadForm" enctype="multipart/form-data">
                        <input id="keyfile" type="file" style="display: none;"/>
                    </form>
                </div>
                <div class="row button">
                    <button class="col keyButton" onclick="gemini()">Gemini Config</button>
                    <button class="col keyButton" onclick="configSSH()" hidden>Manage Keys</button>
                </div>
                <div class="row button">
                    <button class="col keyButton" onclick="logout()">Log Out</button>
                </div>
                <?php require("/var/www/usergen/elements/footer.php"); ?>
            </div>
            <div class="desktopOnly col-4"></div>
        </div>
    </Body>
</HTML>
