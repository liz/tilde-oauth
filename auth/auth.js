var DEBUG = false;
var isMobile = false;
var USE_ORIGIN = "";

const SwalConfig = {
  color: "#79F257",
  background: "#022601",
  buttonsStyling: false,
  showClass: {
    backdrop: 'swal2-noanimation',
    popup: '',
    icon: ''
  },
  hideClass: {
    popup: '',
  }
};

const invalidChars = ["/", "\\", ">", "<", ":", "*", "|", '"', "'", "?", "\0"];

const failMsg = (msg) => {
  Swal.fire({
    ...SwalConfig,
    title: "Error!",
    text: msg,
  });
};

const replaceInvalid = (str) => {
  var cache = str;
  invalidChars.forEach((ch) => {
    cache = cache.replaceAll(ch, "#");
  });
  return cache;
};

const post = (url, data, callback) => {
  var settings = {
    url: url,
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data: data
  };
  $.ajax(settings).done((data) => {
    if (typeof data.token !== "undefined") {
      localStorage.setItem("tty_token", data.token);
    }
    callback(data);
  });
};

const saveFile = (name, type, data) => {
  if (data !== null && navigator.msSaveBlob)
    return navigator.msSaveBlob(new Blob([data], { type: type }), name);
  var a = $("<a style='display: none;'/>");
  var url = window.URL.createObjectURL(new Blob([data], { type: type }));
  a.attr("href", url);
  a.attr("download", name);
  $("body").append(a);
  a[0].click();
  window.URL.revokeObjectURL(url);
  a.remove();
};

const validatePubKey = (key) => {
  return /^(ssh-rsa AAAAB3NzaC1yc2|ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNT|ecdsa-sha2-nistp384 AAAAE2VjZHNhLXNoYTItbmlzdHAzODQAAAAIbmlzdHAzOD|ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1Mj|ssh-ed25519 AAAAC3NzaC1lZDI1NTE5|ssh-dss AAAAB3NzaC1kc3)[0-9A-Za-z+\/]+[=]{0,3}( .*)?$/.test(
    key
  );
};

const sendSSH = (key, id, token) => {
  var payload = {
    pubkey: key,
    token: token,
  };
  post(USE_ORIGIN + "/auth/setKey.php", payload, (response) => {
    if (!response.error) {
      localStorage.setItem("tty_token", response.refreshToken);
      Swal.fire({
        ...SwalConfig,
        title: "Success!",
        text: "Your key has been uploaded to the server.",
      });
    } else {
      Swal.fire({
        ...SwalConfig,
        title: "Failed!",
        text: response.error,
      }).then(() => {
        window.location.reload();
      });
    }
  });
};

const generateSSH = async () => {
  if (window.location.protocol === "http:") {
    Swal.fire({
      ...SwalConfig,
      title: "Error!",
      text: "You must use HTTPS to generate keys.",
    });
    return;
  }
  var token = localStorage.getItem("tty_token");
  var userData = localStorage.getItem("tty_userData");
  if (!token || !userData || token == "undefined" || userData == "undefined") {
    failMsg("Not Logged In");
    return;
  }
  userData = JSON.parse(userData);
  generateKeyPair("RSASSA-PKCS1-v1_5", 4096, "WebGen " + Date())
    .then((keys) => {
      var KeyExport = new JSZip();
      var name = replaceInvalid(userData.username);
      KeyExport.file("HackersTownTTY-" + name, keys[0]);
      KeyExport.file("HackersTownTTY-" + name + ".pub", keys[1]);
      KeyExport.generateAsync({ type: "blob" }).then((content) => {
        saveFile("HackersTownTTY-" + name + ".zip", "application/zip", content);
      });
      sendSSH(keys[1], userData.id, token);
    })
    .catch((err) => {
      console.log(err);
      failMsg("Failed to generate keypair locally.");
    });
};

const testSwal = () => {
  Swal.fire({
    ...SwalConfig,
    title: "Success!",
  });
};

const uploadSSH = () => {
  var token = localStorage.getItem("tty_token");
  var userData = localStorage.getItem("tty_userData");
  if (!token || !userData || token == "undefined" || userData == "undefined") {
    failMsg("Not Logged In");
    return;
  }
  userData = JSON.parse(userData);
  //request local file
  var kf = document.getElementById("keyfile");
  kf.onchange = function (e) {
    // File selected
    var file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.readAsText(file, "UTF-8");
      reader.onload = function (evt) {
        var pubkey = evt.target.result;
        pubkey = pubkey.replace(/\r\n/g, "\n");
        pubkey = pubkey.replace(/\n/g, "");
        if (validatePubKey(pubkey)) {
          sendSSH(pubkey, userData.id, token);
        } else {
          failMsg("Invalid key");
        }
      };
      reader.onerror = function (evt) {
        failMsg("Unable to load Keyfile");
      };
    }
  };
  kf.click();
};

const displayFingerprints = () => {
  // Get SSH Fingerprints and display them
  $.get(USE_ORIGIN + "/fingerprint.php", (response) => {
    if (response) {
      var html =
        '<div><table class="fingerprintTable"><tr><th>Bits</th><th>Fingerprint (SHA256)</th><th class="text-start">Algorithm</th></tr>';
      response.split("\n").forEach((line) => {
        var parts = line.split(" ");
        if (parts.length === 4) {
          html +=
            '<tr><td class="fingerprintBit">' +
            parts[0] +
            '</td><td class="fingerprintData">' +
            parts[1].replace("SHA256:", "") +
            '</td><td class="fingerprintAlgo">' +
            parts[3].replace("(", "").replace(")", "") +
            "</td></tr>";
        }
      });
      html += "</table></div>";
      Swal.fire({
        ...SwalConfig,
        title: "SSH Fingerprints",
        html: html,
      });
    } else {
      Swal.fire({
        ...SwalConfig,
        title: "Unable to Lookup SSH Fingerprints",
        text: response.error,
      });
    }
  });
};

const setUIData = (data) => {
  if (document.getElementById("resizer")) {
    document.getElementById("resizer").innerHTML = data.display_name;
  }
  if(document.getElementById("logo")){
    var logo = document.getElementById("logo");
    logo.src = data.avatar;
    logo.style.borderRadius = "10%";
    logo.alt = "Profile icon for " + data.display_name;
  }
};

const checkLogin = () => {
  // check login status
  var token = localStorage.getItem("tty_token");
  if (token && token !== "undefined"){
    var settings = {
      url: USE_ORIGIN + "/auth/api/index.php",
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: {
        act: "verify",
        token: token
      }
    };
    $.ajax(settings).done(function (response) {
      if(response.error){
        localStorage.removeItem("tty_token");
        localStorage.removeItem("tty_userData");
        window.location.href = "/?error=" + response.error;
      }else {
        localStorage.setItem("tty_token", response.refreshToken);
        localStorage.setItem("tty_userData", JSON.stringify(response.data));
        setUIData(response.data);
      }
    });
  }else {
    window.location.href = "/";
  }
};

const logout = () => {
  localStorage.removeItem("tty_token");
  localStorage.removeItem("tty_userData");
  window.location.href = "/?msg=End%20Of%20Line.";
};

const gemini = () => {
  var payload = {
    token: localStorage.getItem("tty_token"),
    act: "gemproxy",
    enable: "get"
  };
  post(USE_ORIGIN + "/auth/api/index.php", payload, (response) => {
    $.get(USE_ORIGIN + "/auth/gem.html", (ui) => {
      ui = ui.replace("checked", response.data?"checked":"");
      Swal.fire({
        ...SwalConfig,
        title: "Gemini Settings",
        html: ui,
        willClose: (doc) => {
          switch(doc.getElementsByTagName("input")[0].checked){
            case true:
              payload.enable = "yes";
              break;
            case false:
              payload.enable = "no";
              break;
            default:
              break;
          }
          payload.token = localStorage.getItem("tty_token");
          post(USE_ORIGIN + "/auth/api/index.php", payload, (set_response) => {
            if(set_response.error){
              Swal.fire({
                ...SwalConfig,
                title: "Config Failed",
                text: set_response.error,
              });
            }else {
              Swal.fire({
                ...SwalConfig,
                title: "Success",
                text: set_response.data,
              });
            }
          });
        }
      });
    })
  });
};

$(() => {
  // On Page Load
  // Override domain
  $.get("/DOMAIN_OVERRIDE", function (data) {
    USE_ORIGIN = data.replaceAll("\n", "");
    DEBUG = true;
  }).fail(() => {
    USE_ORIGIN = "https://tty.hackers.town";
  }).always(() => {
    // Handle Mobile
    if ( /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i
            .test(navigator.userAgent ) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
            .test(navigator.userAgent.substr(0, 4)
        )) {
        isMobile = true;
    }
    if (isMobile) {
        disableNonDesktopElements();
    }
    // Handle Auth
    var authCode = new URL(window.location.href).searchParams.get("code");
    if (authCode) {
        // Mastodon OAuth2 Redirected here
        var settings = {
          "url": USE_ORIGIN + "/auth/api/index.php",
          "method": "POST",
          "headers": {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          "data": {
            act: "login",
            code: authCode
          }
        };
        $.ajax(settings).done(function (response) {
          if(response.error){
            checkLogin();
          }else{
            localStorage.setItem("tty_token", response.refreshToken);
            localStorage.setItem("tty_userData", JSON.stringify(response.data));
            setUIData(response.data);
          }
        });
    } else {
        // direct connect or from /
        checkLogin();
    }
    // Console Welcome
    console.log("%cWelcome Hacker!", "color: #ff0000; font-size: 7em; font-style: italic; font-family: 'Times New Roman', Times, serif;");
    // Enable Extra Debug Stuff
    if(DEBUG){
      $('.debug').each((i,e)=>{
        e.style.display = "unset";
      });
    }
  });
});
